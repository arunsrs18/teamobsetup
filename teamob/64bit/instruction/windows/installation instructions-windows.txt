*********************************************
## INSTALLATION INSTRUCTIONS FOR WINDOWS ##
*********************************************
1) Download file and extract in your pc.
I assume you saved this in "D:\teamob" folder.

Note: After extract make sure you have below files present
C: or D: > teamOB (Folder)
- Login.jar
- Upload.jar
- windows.bat

2) Open terminal/command prompt and Check for java by writing command "java -version"
3) If Java is not installed download and install from "https://java.com/en/download/"
or https://bitbucket.org/arunsrs18/javasetup/src/master/

4) Right click on windows.bat and select "Run as administrator"
Note: It will open terminal and upon installation it will show login screen.
Close the login screen.

5) Double click on Login.jar. It will ask for activation code.
Enter your unique activation code.

5) Reboot the PC or log off and login again

[CREATE SHORTCUT]
6) Right click on "login.jar" and choose "create shortcut and send to desktop"
8) Goto dashboard and right click on "login.jar" shortcut icon. Rename it if required.
Choose change icon and select the teamob.ico path present in same folder where
both jar files are.


*********************
TROUBLESHOOT::
*********************
1) If login and snap upload does not work in PC. Follow below steps
- Open registry by typing "regedit" in Run
- Goto Local User > Softwares > Microsoft >
Windows > Current version > run
- There should be 2 entries here
D:\TeamOBLogin
D:\TeamOBUpload


2) For windows if teamob was already present you may need to remove entry from registry before
proceeding for new install. To access the registry
1) Open command prompt
2) Run "regedit"
3) Within registry window
Current users or Local users > Softwares > Microsoft >
Windows > Current version > run > projectob/teamOB
(Right click and delete the existing entry)

3) If troubleshooting required in login.jar. Open command prompt and run the build as
java -jar /va/path of login.jar

4) If troubleshooting required in Upload.jar. Open command prompt and run the build as
java -jar /va/path of Upload.jar

******
Note:
******
1) If jar show errors in windows /Temp/ folder for dll load.
Simply delete the temp folder

2) If jar show bind error that means 2 instance of login.jar has been executed.
Solution is to reboot the pc.


