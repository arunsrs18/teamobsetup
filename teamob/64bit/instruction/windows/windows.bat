@echo off
:: BatchGotAdmin (Run as Admin code starts)
REM --> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
echo Requesting administrative privileges...
goto UACPrompt
) else ( goto gotAdmin )
:UACPrompt
echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"
"%temp%\getadmin.vbs"
exit /B
:gotAdmin
if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
pushd "%CD%"
CD /D "%~dp0"
:: BatchGotAdmin (Run as Admin code ends)
:: Your codes should start from the following line
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /V "TeamOBUpload" /t REG_SZ /F /D "%~dp0Upload.jar"
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /V "TeamOBLogin" /t REG_SZ /F /D "%~dp0Login.jar"
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /V "TeamUpload" /t REG_SZ /F /D "%~dp0Upload.jar"
@echo off
attrib +h "%~dp0.ob"
start "" cmd /c "echo Installation Successfull. Please restart your PC!&echo(&pause"
